#!/usr/bin/env gnuplot
plot 'fig5.dat' u 1:2 smooth bezier notitle
set xrange [0.36:1.4]
set yrange [0.35:1.5]
set xlabel 'D_{ep}'
set ylabel 'D_{p}'

set label "PROPAGATION" at  graph 0.5,0.5 font "DejaVuSans,16"
set label "NO PROPAGATION" at  graph 0.05,0.15 font "DejaVuSans,16"

set term eps enhanced font "DejaVuSans,16"
set output "fig5.eps"
replot
